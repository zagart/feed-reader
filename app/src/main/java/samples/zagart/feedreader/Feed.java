package samples.zagart.feedreader;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings({"WeakerAccess", "unused"})
class Feed {

    @SerializedName("link_id")
    private Long mId;

    @SerializedName("title")
    private String mTitle;

    public Long getId() {
        return mId;
    }

    public void setId(final Long pId) {
        mId = pId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(final String pTitle) {
        mTitle = pTitle;
    }

    @Override
    public boolean equals(final Object pO) {
        if (this == pO) return true;
        if (pO == null || getClass() != pO.getClass()) return false;

        final Feed feed = (Feed) pO;

        return mId != null ? mId.equals(feed.mId) : feed.mId == null
                && (mTitle != null ? mTitle.equals(feed.mTitle) : feed.mTitle == null);

    }

    @Override
    public int hashCode() {
        int result = mId != null ? mId.hashCode() : 0;
        result = 31 * result + (mTitle != null ? mTitle.hashCode() : 0);
        return result;
    }
}

package samples.zagart.feedreader;

import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class FeedAdapter extends RecyclerView.Adapter {

    private final List<Feed> mItems = new ArrayList<>();

    @UiThread
    void addFeeds(final List<Feed> pFeeds) {
        for (final Feed feed : pFeeds) {
            if (!mItems.contains(feed)) {
                mItems.add(feed);
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup pParent, final int pViewType) {
        final LayoutInflater inflater = LayoutInflater.from(pParent.getContext());
        final View itemView = inflater.inflate(R.layout.view_feed, pParent, false);

        return new FeedHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder pHolder, final int pPosition) {
        ((FeedHolder) pHolder).mTitleView.setText(mItems.get(pPosition).getTitle());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    private class FeedHolder extends RecyclerView.ViewHolder {

        final TextView mTitleView;

        FeedHolder(final View mItem) {
            super(mItem);

            mTitleView = (TextView) mItem.findViewById(R.id.feed_title);
        }
    }
}

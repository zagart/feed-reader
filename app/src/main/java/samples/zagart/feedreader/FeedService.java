package samples.zagart.feedreader;

import android.support.annotation.StringDef;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface that describes methods that execute API calls to feed service.
 * <p>
 * Created by zagart on 10.05.17.
 */
@SuppressWarnings("WeakerAccess")
public interface FeedService {
    String SERVICE_ENDPOINT = "https://refind.com";

    @GET("/chrismessina.json?")
    Call<List<Feed>> getFeed(@Query("sort") final String pSort,
                             @Query("days") final int pDays,
                             @Query("amount") final int pAmount);

    @SuppressWarnings("UnnecessaryInterfaceModifier")
    public class Impl {

        private static Impl INSTANCE = new Impl();

        public static Impl get() {
            return INSTANCE;
        }

        public void getFeeds(final RecyclerView pRecyclerView,
                             @Query("sort") @Sort final String pSort,
                             @Query("days") final int pDays,
                             @Query("amount") final int pAmount) {
            if (pRecyclerView == null || TextUtils.isEmpty(pSort)) {
                return;
            }

            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(SERVICE_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            final FeedService service = retrofit.create(FeedService.class);
            final Call<List<Feed>> call = service.getFeed(pSort, pDays, pAmount);

            try {
                final List<Feed> feeds = call.execute().body();

                pRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        ((FeedAdapter) pRecyclerView.getAdapter()).addFeeds(feeds);
                    }
                });
            } catch (final IOException pEx) {
                Log.e(FeedService.class.getSimpleName(), pEx.getMessage());
            }
        }
    }

    @StringDef({
            Sort.TOP,
            Sort.LATEST
    })
    @interface Sort {
        String TOP = "top";
        String LATEST = "latest";
    }
}

package samples.zagart.feedreader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    public static final int DAYS_LIMIT = 30;
    public static final int REFRESH_INTERVAL = 1;
    public static final int FEEDS_LIMIT = 200;

    private Observable<Long> mFeedsObservable = Observable.interval(REFRESH_INTERVAL, TimeUnit.SECONDS).limit(FEEDS_LIMIT);
    private Scheduler mBackgroundThread = Schedulers.from(Executors.newSingleThreadExecutor());
    private Scheduler mUIThread = Schedulers.io();

    @Override
    protected void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);

        setContentView(R.layout.activity_main);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new FeedAdapter());

        mFeedsObservable
                .subscribeOn(mBackgroundThread)
                .observeOn(mUIThread)
                .subscribe(new SimpleSubscriber<Long>() {

                    @Override
                    public void onNext(final Long pFeedsAmount) {
                        FeedService.Impl.get().getFeeds(
                                recyclerView,
                                FeedService.Sort.LATEST,
                                DAYS_LIMIT,
                                pFeedsAmount.intValue()
                        );
                    }
                });
    }

    abstract private class SimpleSubscriber<T> extends Subscriber<T> {

        @Override
        public void onCompleted() {
            //no action required
        }

        @Override
        public void onError(final Throwable pEx) {
            Log.e(MainActivity.class.getSimpleName(), String.valueOf(pEx));
        }
    }
}
